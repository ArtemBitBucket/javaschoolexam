package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
        // TODO: Implement the logic here
        if (x == null || y == null){
            throw new IllegalArgumentException();
        }
        Iterator xIterator = x.iterator();
        Iterator yIterator = y.iterator();
        String xToken;
        String yToken;
        while (xIterator.hasNext()){
            xToken = xIterator.next().toString();
            while (yIterator.hasNext()){
                yToken = yIterator.next().toString();
                if (xToken.equals(yToken)){
                    break;
                }
            }
            if (!yIterator.hasNext() && xIterator.hasNext()){
                return false;
            }
        }
        return true;
    }
}
