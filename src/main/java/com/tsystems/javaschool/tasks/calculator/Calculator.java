package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.StringTokenizer;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        Queue output = shuntingYard (statement);
        return execStatement(output);
    }
    
    private String execStatement(Queue<String> queueStatement){
        
        if (queueStatement == null) {
            return null;
        } 
        
        int opArgCount = 2;
        String currentToken;
        Stack<BigDecimal> stack = new Stack<>();
        BigDecimal a;
        BigDecimal b;
        BigDecimal result = new BigDecimal(0);
        // ���� �� ����� �������� ������
        while(queueStatement.size() > 0){
            // ��������� ��������� �����
            currentToken = queueStatement.poll();
            // ���� ����� - �������� ��� �������������
            if(isNumber(currentToken)){
                // ��������� ��� � ����
                stack.push(BigDecimal.valueOf(Double.valueOf(currentToken)));
            }
            // � ��������� ������, ����� - �������� (����� ��� ���������� ���������� ��� ��������, ��� � �������� �������)
            else if(isOperator(currentToken)){
                // ������� ��������, ��� �������� ��������� n ����������
                // ���� � ����� �������� ������, ��� n
                if(stack.size() < opArgCount){
                    // (������) ������������� ���������� ���������� � ���������.
                    return null;
                }
                
                b = stack.pop();
                a = stack.pop();
                switch (currentToken){
                    case "+":
                        result = a.add(b);
                        break;
                    case "-":
                        result = a.subtract(b);
                        break;
                    case "*":
                        result = a.multiply(b);
                        break;
                    case "/":
                        if (b.compareTo(BigDecimal.ZERO) != 0){
                            result = a.divide(b, 6, RoundingMode.CEILING);
                        }
                        else {
                            return null;
                        }
                        break;
                }
                // ���� �������� �������������� ��������, ��������� ������� � ����.
                stack.push(result);
            }
        }
        // ���� � ����� �������� ���� ���� ��������,
        // ��� ����� �������� ����������� ����������.
        if(stack.size() == 1){
            result = stack.pop();
            if (result.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) == 0 ){
                return String.valueOf(result.intValue());
            }
            
            return result.setScale(4, RoundingMode.HALF_DOWN).stripTrailingZeros().toString();
        }
        // ���� � ����� ������� ���������� ��������,
        // (������) ������������ ��� ������� ����� ��������.
        return null;
    }
    
    private Queue<String> shuntingYard (String statement){
        
        if (statement == null) {
            return null;
        } 
        
        StringTokenizer sToken = new StringTokenizer(statement,"+-*/()",true);
        Queue output = new LinkedList();
        Stack<String> stack = new Stack<>();
        String currentToken;
        
        while (sToken.hasMoreTokens()){
            currentToken = sToken.nextToken();
            if (isNumber(currentToken)){
                output.add(currentToken);
            }
            else if (isOperator(currentToken)){
                while(!stack.isEmpty()){
                    if(isOperator(stack.peek()) &&
                        (opPrecedency(currentToken) <= opPrecedency(stack.peek() ))) {
                        output.add(stack.pop());
                    }
                    else{
                        break;
                    }
                }
                stack.push(currentToken);
            }
            // ���� ����� - ����� ������� ������, �� �������� ��� � ����.
            else if(currentToken.equals("(")){
                stack.push(currentToken);
            }
            // ���� ����� - ������ ������� ������:
            else if(currentToken.equals(")")){
                Boolean pe = false;
                // �� ��������� �� ������� ����� ������ "����� ������� ������"
                // ������������� ��������� �� ����� � ������� ������.
                while(!stack.isEmpty()){
                    if(stack.peek().equals("(")){
                        pe = true;
                        break;
                    }
                    else{
                        output.add(stack.pop());
                    }
                }
                // ���� ���� �������� �� ���������� ������ ����� ������� ������, �� ���� ��������� ������.
                if(!pe){
                    return null;
                }
                // ���������� ����� "����� ������� ������" �� ����� (�� ��������� � ������� ������).
                stack.pop();
            }
            else{
                return null;
            }
        }
        while(!stack.isEmpty()){
            if(stack.peek().equals("(") || stack.peek().equals(")")){
                return null;
            }
            output.add(stack.pop());    
        }
        return output;
    }
    
    private boolean isNumber(String token){
        try{
            Double.parseDouble(token);
            return true;
        }catch(NumberFormatException e){
            return false;
        }
    }
    
    private boolean isOperator(String token){
        return token.equals("+") || token.equals("-") || 
               token.equals("*") || token.equals("/");
    }
    
    private int opPrecedency(String token){
        switch (token){
            case "*":
            case "/":
                return 3;
            case "+":
            case "-":
                return 2;
            case "=":
                return 1;    
        }
        return 0;
    }
}
