package com.tsystems.javaschool.tasks.pyramid;

import static java.lang.Math.sqrt;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException{
        // TODO : Implement your solution here
        if (inputNumbers.size() > 120){
            throw new CannotBuildPyramidException();
        }
        for (Integer number : inputNumbers){
            if (number == null){
                throw new CannotBuildPyramidException();
            }
        }
        inputNumbers.sort(null);
        Integer rowCount = calcRowCount(inputNumbers.size());
        Integer columnCount = rowCount + (rowCount - 1);
        Integer centralColumnNum = rowCount - 1;
        Integer iterator = 0;
        int[][] pyramid = new int[rowCount][columnCount];
        
        for (int row = 0; row < rowCount; row++){
            int position = centralColumnNum - row;
            for(int step = 0; step < (row + 1); step++){
                pyramid[row][position] = inputNumbers.get(iterator);
                position = position + 2;
                iterator++;
            }
        }
        return pyramid;
    }

    private Integer calcRowCount(Integer ElementsCount){
        //total sum of progression
        //ElementsCount = (n*(n+1))/2
        //or 
        //n^2 + n - 2*ElementsCount = 0
        //discriminant
        Integer d = 1 + 4*ElementsCount*2;
        //positive root
        Integer rowCount = (int)Math.round((-1 + sqrt(d))/2);
        return rowCount;
    }
}
